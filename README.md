*INTRODUCTION:*

   Automation testing of different REST API requests such as POST,PUT,PATCH,GET and DELETE.

*FEATURES:*

 1.Here we used sample free APIs from reqres.in website for api testing demonstartion.
 2.Created environment as reqres_env,used environment variables and executed all APIs in that environment.
 3.Used different testing methods such as data driven testing using json data file and excel data file.
   tested APIs using dyanamic varaiables also
 4.Used concept of request chaining in one of the POST API ,executed for 5 times and then terminated the loop   

*REPORT:*

CollectioN : rest_allApi_configuration Copy ,
 Exported withNewman v6.1.0 
 
   iterations :pass-1  fail-0,
   Requests :pass-17  fail-0 ,               
   Prerequest Scripts :pass-19 fail-0,
   Test Scripts:pass-33 fail-0,
   Assertions :pass-77 fail-0
